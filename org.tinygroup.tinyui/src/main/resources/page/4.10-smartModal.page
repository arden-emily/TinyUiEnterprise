#import("/org/tinygroup/modal/components/modal_smart.component")
#@h2()4.10.2 弹出框（Popover）#end
        #@p()弹出框（Popover）插件根据需求生成内容和标记，默认情况下是把弹出框（popover）放在它们的触发元素后面。#end
        #@exampleView()
			#include("/page/modal/10.2.page")
        #end
        
        <ul class="list"> <li><b>通过 data 属性</b>：如需添加一个弹出框（popover），只需向一个锚/按钮标签添加 <b>data-toggle="popover"</b> 即可。锚的 title 即为弹出框（popover）的文本。默认情况下，插件把弹出框（popover）设置在顶部。 <pre class="prettyprint prettyprinted" style="background-image: none;"><span class="tag">&lt;a</span><span class="pln"> </span><span class="atn">href</span><span class="pun">=</span><span class="atv">"#"</span><span class="pln"> </span><span class="atn">data-toggle</span><span class="pun">=</span><span class="atv">"popover"</span><span class="pln"> </span><span class="atn">title</span><span class="pun">=</span><span class="atv">"Example popover"</span><span class="tag">&gt;</span><span class="pln">
   请悬停在我的上面
</span><span class="tag">&lt;/a&gt;</span></pre> </li> <li><b>通过 JavaScript：</b>通过 JavaScript 启用弹出框（popover）： <pre class="prettyprint prettyprinted" style="background-image: none;"><span class="pln">$</span><span class="pun">(</span><span class="str">'#identifier'</span><span class="pun">).</span><span class="pln">popover</span><span class="pun">(</span><span class="pln">options</span><span class="pun">)</span></pre> </li> </ul>
        <p>弹出框（Popover）插件不像之前所讨论的下拉菜单及其他插件那样，它不是纯 CSS 插件。如需使用该插件，您必须使用 jquery 激活它（读取 javascript）。使用下面的脚本来启用页面中的所有的弹出框（popover）： </p>
        <pre class="prettyprint prettyprinted" style="background-image: none;"><span class="pln">$</span><span class="pun">(</span><span class="kwd">function</span><span class="pln"> </span><span class="pun">()</span><span class="pln"> </span><span class="pun">{</span><span class="pln"> $</span><span class="pun">(</span><span class="str">"[data-toggle='popover']"</span><span class="pun">).</span><span class="pln">popover</span><span class="pun">();</span><span class="pln"> </span><span class="pun">});</span></pre>
        
        #@h3()Html代码#end
        #@htmlSyntaxHighlighter()
        	
        	<div class="container" style="padding: 20px 50px 10px;" >
        		/*悬停左侧组件*/
			   <button type="button" class="btn btn-default" title="Popover title" data-container="body" data-toggle="popover" data-placement="left" data-content="左侧的 Popover 中的一些内容">
			      左侧的 Popover
			   </button>
			   /*悬停顶部组件*/
			   <button type="button" class="btn btn-primary" title="Popover title" data-container="body" data-toggle="popover" data-placement="top" data-content="顶部的 Popover 中的一些内容">
			      顶部的 Popover
			   </button>
			   /*悬停底部组件*/
			   <button type="button" class="btn btn-success" title="Popover title" data-container="body" data-toggle="popover" data-placement="bottom" data-content="底部的 Popover 中的一些内容">
			      底部的 Popover
			   </button>
			   /*悬停右侧组件*/
			   <button type="button" class="btn btn-warning" title="Popover title" data-container="body" data-toggle="popover" data-placement="right" data-content="右侧的 Popover 中的一些内容">
			      右侧的 Popover
			   </button>
		   </div>
        #end
        #macroCode("/page/modal/10.2.page")
        #@h2()4.10.3 工具提示（Tooltip）#end
        #@p()工具提示（Tooltip）插件根据需求生成内容和标记，默认情况下是把工具提示（tooltip）放在它们的触发元素后面。#end
        #@exampleView()
			#include("/page/modal/10.3.page")
        #end
        
        <ul class="list"> <li><b>通过 data 属性</b>：如需添加一个工具提示（tooltip），只需向一个锚标签添加 <b>data-toggle="tooltip"</b> 即可。锚的 title 即为工具提示（tooltip）的文本。默认情况下，插件把工具提示（tooltip）设置在顶部。 <pre class="prettyprint prettyprinted" style="background-image: none;"><span class="tag">&lt;a</span><span class="pln"> </span><span class="atn">href</span><span class="pun">=</span><span class="atv">"#"</span><span class="pln"> </span><span class="atn">data-toggle</span><span class="pun">=</span><span class="atv">"tooltip"</span><span class="pln"> </span><span class="atn">title</span><span class="pun">=</span><span class="atv">"Example tooltip"</span><span class="tag">&gt;</span><span class="pln">请悬停在我的上面</span><span class="tag">&lt;/a&gt;</span></pre> </li> <li><b>通过 JavaScript</b>：通过 JavaScript 触发工具提示（tooltip）： <pre class="prettyprint prettyprinted" style="background-image: none;"><span class="pln">$</span><span class="pun">(</span><span class="str">'#identifier'</span><span class="pun">).</span><span class="pln">tooltip</span><span class="pun">(</span><span class="pln">options</span><span class="pun">)</span></pre> </li> </ul>
        <p><img src="${TINY_CONTEXT_PATH}/img/quote.png">工具提示（Tooltip）插件不像之前所讨论的下拉菜单及其他插件那样，它不是纯 CSS 插件。如需使用该插件，您必须使用 jquery 激活它（读取 javascript）。使用下面的脚本来启用页面中的所有的工具提示（tooltip）： <pre class="prettyprint prettyprinted" style="background-image: none;"><span class="pln">$</span><span class="pun">(</span><span class="kwd">function</span><span class="pln"> </span><span class="pun">()</span><span class="pln"> </span><span class="pun">{</span><span class="pln"> $</span><span class="pun">(</span><span class="str">"[data-toggle='tooltip']"</span><span class="pun">).</span><span class="pln">tooltip</span><span class="pun">();</span><span class="pln"> </span><span class="pun">});</span></pre> </p>
        
        #@h3()Html代码#end
        #@htmlSyntaxHighlighter()
        	/*默认 Tooltip组件*/
        	<button type="button" class="btn btn-default" data-toggle="tooltip" title="默认的 Tooltip">
			   默认的 Tooltip
			</button>
			/*左侧Tooltip组件*/
			<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="左侧的 Tooltip">
			   左侧的 Tooltip
			</button>
			/*顶部 Tooltip组件*/
			<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="顶部的 Tooltip">
			   顶部的 Tooltip
			</button>
			/*底部 Tooltip组件*/
			<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="底部的 Tooltip">
			   底部的 Tooltip
			</button>
			/*右侧 Tooltip组件*/
			<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right" title="右侧的 Tooltip">
			   右侧的 Tooltip
			</button>
        #end
        #macroCode("/page/modal/10.3.page")