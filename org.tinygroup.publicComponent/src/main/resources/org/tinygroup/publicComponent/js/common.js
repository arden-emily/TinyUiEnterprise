jQuery.Huitab =function(tabBar,tabCon,class_name,tabEvent,i){
	var $tab_menu=$(tabBar);
	// 初始化操作
	$tab_menu.removeClass(class_name);
	$(tabBar).eq(i).addClass(class_name);
	$(tabCon).hide();
	$(tabCon).eq(i).show();
		
	$tab_menu.bind(tabEvent,function(){
		$tab_menu.removeClass(class_name);
		$(this).addClass(class_name);
		var index=$tab_menu.index(this);
		$(tabCon).hide();
		$(tabCon).eq(index).show();
	});
}
// JavaScript Document
$(function(){
	$("#nav-toggle").click(function(){
		$(".Tiny_aside").slideToggle();		
	});	
	
	$(".dislpayArrow a").click(function(){
		if($(".Tiny_aside").is(":hidden")){
			$(".Tiny_aside").show();$(this).removeClass("open");
			$(".Tiny_article,.dislpayArrow").css({"left":"200px"});
		}else{
			$(this).addClass("open");
			$(".Tiny_aside").hide();
			$(".Tiny_article,.dislpayArrow").css({"left":"0"});
		}
	});
	
	$.Huitab("#websafecolor .colortab span","#websafecolor .colorcon","selected","click","0");
	$.Huitab("#tab_demo .tabBar span","#tab_demo .tabCon","Current","click","0");
	$.Huitab("#tab_demo2 .tabBar span","#tab_demo2 .tabCon","Current","mousemove","1");
	$.Huitab("#bgpic .colortab span","#bgpic .colorcon","selected","click","0");

	$.Huihover('.codeView');
}); 