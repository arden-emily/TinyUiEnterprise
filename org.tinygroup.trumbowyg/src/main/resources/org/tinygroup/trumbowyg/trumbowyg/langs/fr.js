/* ===========================================================
 * fr.js
 * French translation for Trumbowyg
 * http://alex-d.github.com/Trumbowyg
 * ===========================================================
 * Author : Alexandre Demode (Alex-D)
 *          Twitter : @AlexandreDemode
 *          Website : alex-d.fr
 */

$.trumbowyg.langs.fr = {
    viewHTML:       "查看HTML",

    formatting:     "格式",
    p:              "段落",
    blockquote:     "引用",
    code:           "代码",
    header:         "纤度",

    bold:           "粗体",
    italic:         "斜体",
    strikethrough:  "加删除线",
    underline:      "下划线",

    strong:         "加粗",
    em:             "Emphase",
    del:            "删除",

    unorderedList:  "无序列表",
    orderedList:    "有序列表",

    insertImage:    "插入图片",
    insertVideo:    "插入视频",
    link:           "链接",
    createLink:     "创建",
    unlink:         "拆开",

    justifyLeft:    "居左",
    justifyCenter:  "居中",
    justifyRight:   "居右",
    justifyFull:    "全部对齐",

    horizontalRule: "分割线",

    fullscreen:     "全屏",

    close:          "关闭",

    submit:         "确定",
    reset:          "取消",

    invalidUrl:     "无效的Url",
    required:       "必要的",
    description:    "描述",
    title:          "标题",
    text:           "文本"
}